#! /bin/bash
#SBATCH -J newcpgreport
#SBATCH -o output.out
#SBATCH -e error.out
#SBATCH --mem=16G
#SBATCH --mail-type=END,FAIL
#Purge any previous modules
module purge
module load bioinfo/EMBOSS-6.6.0
newcpgreport -sequence Coturnix_japonica.Coturnix_japonica_2.0.dna.toplevel.fa -window 100 -shift 1 -minlen 200 -minoe 0.6 -minpc 50. -outfile Coturnix_japonica_2.0_CGI.newcpgreport
