#! /usr/bin/perl

use Bio::EnsEMBL::Registry;

my $registry = 'Bio::EnsEMBL::Registry';

$registry->load_registry_from_db(
  -host => 'ensembldb.ensembl.org',
  -user => 'anonymous',
);

my $simple_feature_adaptor = $registry->get_adaptor( 'coturnix_japonica', 'Core', 'SimpleFeature' );

my $cpgi = $simple_feature_adaptor->fetch_all_by_logic_name( 'cpg' );

foreach my $cpg (@$cpgi) {
    print join("\t",$cpg->seq_region_name,$cpg->start - 1,$cpg->end - 1)."\n";
}
