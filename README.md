# Option 1: Download the CpG island track from Ensembl

Many thanks to Cedric Cabau for his help with the perl script.

## dependancy
Good luck installing the Ensembl perl API. See the relevant link to assist you:
- [API installation](https://www.ensembl.org/info/docs/api/api_installation.html)
- [Debugging API installation](https://www.ensembl.org/info/docs/api/debug_installation_guide.html) (you are likely to be missing a few system dependencies)
 
## Get the 'Ensembl' name for your species

```bash
./get_ensembl_species.pl > myspecies.txt
```

Then open `myspecies.txt` and search for your favourit species.

## Get the CpG island track as a 3 column BED file
Edit line 12 of the `get_ensembl_cgi.pl` script with your favourite species:

```perl
my $simple_feature_adaptor = $registry->get_adaptor( 'coturnix_japonica', 'Core', 'SimpleFeature' );
```

And run the script:

```bash
./get_ensembl_cgi.pl > CoJa2.0_Ensembl_CGI.bed
```

In the following image, we compare the resulting track (brown) against the native Ensembl track (pink). Everything seems in order.

![Ensembl vs Ensembl](Ensembl_CGI.png)


# Option 2: de novo CpG island detection

Detection of CpG island on a `.fasta` genome, using EMBOSS `newcpgreport`.
Then parsing of the `.newcpgreport` output using R, and export in `.bed` and `.tsv`. The `.tsv` file has slightly more usefull columns.

## step 1
Download and/or access a fasta file of your favourite genome (for example from Ensembl). It should be decompressed:

```bash
wget https://ftp.ensembl.org/pub/release-108/fasta/coturnix_japonica/dna/Coturnix_japonica.Coturnix_japonica_2.0.dna.toplevel.fa.gz
gzip -d Coturnix_japonica.Coturnix_japonica_2.0.dna.toplevel.fa.gz
```

## step 2

Edit file names and paths in the file `1_newcpgreport.sh`. Make it executible:
```bash
chmod u+x 1_newcpgreport.sh
```

and launch the job on genotoul:

```bash
sbatch 1_newcpgreport.sh 

```

## step 3
Open the script `2_parsing_newcpgreport.R` in your favourite IDE, edit paths, and launch the script.
It migh be best to run the script interactively to catch bugs early.

The script shoud produce a `.bed` file:

```bash
head Coturnix_japonica_2.0_cgi.bed 
1	4768	5895	.	0	.
1	19663	19905	.	0	.
1	22151	22405	.	0	.
1	22944	23438	.	0	.
1	23485	23957	.	0	.
1	24192	24471	.	0	.
1	25567	25861	.	0	.
```

and a more informative `.tsv` file:

```bash
head Coturnix_japonica_2.0_cgi.tsv
chr	start	end	width	percentCG	ObsExp
1	4769	5895	1126	72.05	0.85
1	19664	19905	241	73.97	0.73
1	22152	22405	253	69.69	0.68
1	22945	23438	493	60.32	0.97
1	23486	23957	471	72.88	0.67
1	24193	24471	278	71.33	0.72
1	25568	25861	293	65.99	0.74
```

(notice some 'off-by-one' differences due to the `.bed` format starting to count at 0 while R starts to count at 1 like a normal person)

## The bad news

It seems EMBOSS default values for `newcpgreport` (brown) do not faithfully reproduce Ensembl CpG isand track (magenta).

![newcpgreport vs Ensembl](CGI_Ensembl_vs_EMBOSS.png)

Maybe different settings would work better? The shorted Ensembl CGI is 400 bp, so setting `minsize 400` in `1_newcpgrepports.sh` might help.
